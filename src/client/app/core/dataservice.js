(function() {
  'use strict';

  angular
    .module('app.core')
    .factory('dataservice', dataservice);

  dataservice.$inject = ['$http', '$q', 'exception', 'logger'];
  /* @ngInject */
  function dataservice($http, $q, exception, logger) {
    var service = {
      delay: delay,
      getTodoList: getTodoList,
      updateTodoList: updateTodoList,
      updateTodo: updateTodo,
      deleteTodo: deleteTodo,
      getTodoIndex: getTodoIndex
    };

    return service;

    /**
     * Simulates an async call with the millis passed
     * @param  {Integer} ms Time to delay
     * @return {Object}  $q Promise
     */
    function delay(ms, callback) {
      var deferred = $q.defer();
      setTimeout(function() {
        deferred.resolve(callback());
      }, ms);
      return deferred.promise;
    }

    /**
     * Gets the data from the local st0rage
     * @return {Array} Todo list
     */
    function getTodoList() {
      return localStorage.todos ? JSON.parse(localStorage.todos) : []
    }

    /**
     * Updates the local storage list
     * @param  {Array} list List to be saved in the local storage
     */
    function updateTodoList(list) {
        localStorage.todos = list ? JSON.stringify(list) : JSON.stringify([])
    }

    /**
     * Updates a todo element in the list
     * @param  {String} id   Id of the todo to update
     * @param  {Object} todo Todo item to update
     */
    function updateTodo(id, todo) {
      var list = getTodoList()
      updateTodoList(list.map(function(item) {
        if (item.id === id) {
          return todo
        }
        return item
      }))
    }

    /**
     * Updates a todo element in the list
     * @param  {[type]} id   [description]
     * @param  {[type]} todo [description]
     * @return {[type]}      [description]
     */
    function deleteTodo(id) {
      var list = getTodoList()
      list.splice(getTodoIndex(list, id), 1)
      updateTodoList(list)
    }

    /**
     * Obtain the index of a todo of the list passed
     * @param  {Array} list List to search
     * @param  {String} id   Id to search
     * @return {Integer} The index of the todo in the list
     */
    function getTodoIndex(list, id) {
      for(var i = 0; i < list.length; i++)
      {
        if(list[i].id === id)  {
          return i
        }
      }
    }
  }
})();
