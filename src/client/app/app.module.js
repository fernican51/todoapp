(function() {
  'use strict';

  angular.module('app', [
    'app.core',
    'app.widgets',
    'app.main',
    'app.layout',
    'app.todos'
  ]);

})();
