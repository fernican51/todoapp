(function() {
  'use strict';

  angular
    .module('app.todos')
    .controller('TodosController', TodosController);

  TodosController.$inject = ['$q', 'dataservice', 'logger', 'todoservice'];
  /* @ngInject */
  function TodosController($q, dataservice, logger, todoservice) {
    var todos = this;
    todos.newTodo = {}
    todos.isFetching = todoservice.isFetching
    todos.list = todoservice.list

    activate();

    function activate() {
      todoservice.fetch()
      resetNewTodo()
    }

    function resetNewTodo() {
      todos.newTodo = {
        title: '',
        completed: false
      }
    }

    todos.addTodo = function() {
      if(todos.newTodo.title)
      {
        todoservice.add(angular.copy(todos.newTodo))
        resetNewTodo()
      } else {
        logger.error('You must provide a value')
      }
    }
  }
})();
