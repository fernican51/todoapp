(function() {
  'use strict';

  angular
    .module('app.todos')
    .factory('todoservice', todoservice);

  todoservice.$inject = ['$q', 'exception', 'logger', 'uuid4', 'dataservice'];
  /* @ngInject */
  function todoservice($q, exception, logger, uuid4, dataservice) {
    var todos = []
    var isFetching = true
    var service = {
      list: getTodos,
      fetch: fetch,
      add: add,
      remove: remove,
      toggleCompleted: toggleCompleted,
      isFetching: getIsFetching
    };

    return service;

    function getTodos() {
      return todos;
    }

    function getIsFetching() {
      return isFetching;
    }

    function toggleCompleted(todo) {
      isFetching = true;
      return dataservice.delay(500, function() {
        return true
      }).then(function(data) {
        todo.completed = !todo.completed
        dataservice.updateTodo(todo.id, todo)
        isFetching = false;
        logger.success('Item updated')
      });
    }

    function addTodo(todo) {
      var list = dataservice.getTodoList()
      todo.id = uuid4.generate()
      todo.createdAt = new Date()
      list.push(todo);
      dataservice.updateTodoList(list)
      return todo
    }

    function fetch() {
      isFetching = true;
      return dataservice.delay(1000, function() {
        return dataservice.getTodoList()
      }).then(function(data) {
        todos = data
        isFetching = false;
      });
    }

    function add(todo) {
      isFetching = true;
      return dataservice.delay(500, function() {
        return addTodo(todo)
      }).then(function(todo) {
        todos.push(todo)
        isFetching = false;
        logger.success('Item added')
      });
    }

    function update(todo) {
      isFetching = true;
      return dataservice.delay(500, function() {
        return dataservice.updateTodo(todo)
      }).then(function(todo) {
        todos[dataservice.getTodoIndex(todos, todo.id)] = todo
        isFetching = false;
        logger.success('Item updated')
      });
    }

    function remove(todo) {
      isFetching = true;
      return dataservice.delay(500, function() {
        return dataservice.deleteTodo(todo.id)
      }).then(function() {
        todos.splice(dataservice.getTodoIndex(todos, todo.id), 1)
        isFetching = false;
        logger.success('Item removed')
      });
    }
  }
})();
