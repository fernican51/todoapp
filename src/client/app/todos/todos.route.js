(function() {
  'use strict';

  angular
    .module('app.todos')
    .run(appRun);

  appRun.$inject = ['routerHelper'];
  /* @ngInject */
  function appRun(routerHelper) {
    routerHelper.configureStates(getStates());
  }

  function getStates() {
    return [
      {
        state: 'todos',
        config: {
          url: '/todos',
          templateUrl: 'app/todos/todos.html',
          controller: 'TodosController',
          controllerAs: 'todos',
          title: 'todos',
          settings: {
            nav: 2,
            content: '<i class="fa fa-dashboard"></i> Todos'
          }
        }
      }
    ];
  }
})();
