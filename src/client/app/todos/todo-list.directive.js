(function() {
    'use strict';

    angular
        .module('app.todos')
        .directive('todoList', todoList);

    /* @ngInject */
    function todoList () {
        var directive = {
            bindToController: true,
            controller: todoListController,
            controllerAs: 'vm',
            scope: {
                'list': '='
            },
            templateUrl: 'app/todos/todo-list.html'
        };

        /* @ngInject */
        function todoListController(logger, todoservice) {
            var vm = this;
            vm.removeItem = todoservice.remove
            vm.toggleCompleted = todoservice.toggleCompleted
        }

        return directive;
    }
})();
